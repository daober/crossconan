from conans import ConanFile, CMake

class CrossHello(ConanFile):
    name = "CrossHello"
    version = "0.1.0"
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake" 

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
